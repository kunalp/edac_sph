"""Simple KHI implementation using the EDAC formulation."""

# numpy
import numpy as np

# PySPH imports
from pysph.base.nnps import DomainManager
from pysph.base.kernels import CubicSpline
from pysph.solver.application import Application

# the equations
from edac import get_particle_array_edac, get_particle_array_edac_solid
from edac import EDACScheme


# domain and reference values
L = 1.0; Umax = 1.0
c0 = 10 * Umax; rho0 = 1.0
co = c0
p0 = c0*c0*rho0

# Reynolds number and kinematic viscosity
Re = 100; nu = Umax * L/Re

# Numerical setup
nx = 50; dx = L/nx
ghost_extent = 5 * dx
hdx = 1.3

# adaptive time steps
h0 = hdx * dx
dt_cfl = 0.25 * h0/( c0 + Umax )
dt_viscous = 0.125 * h0**2/nu
dt_force = 1.0

tf = 10.0
dt = 0.75 * min(dt_cfl, dt_viscous, dt_force)


class KHI(Application):

    def create_particles(self):

        # The fluid.
        Lb2 = 0.5*L
        x, y = np.mgrid[-Lb2:Lb2:nx*1j, -Lb2:Lb2:nx*1j]
        x, y = (np.ravel(_arr) for _arr in (x,y))
        fluid = get_particle_array_edac(name='fluid', x=x, y=y)
        fluid.add_property('color')
        # add the output arrays
        fluid.add_output_arrays( ['color'] )

        # Setup the interface and the properties below and above it.
        interface = 0.05*np.sin(2*x*np.pi/L)

        fluid.u[y > interface] = 0.5
        fluid.color[y > interface] = 1
        fluid.u[y < interface] = -0.5

        # Now setup the solid.
        xt, yt = np.mgrid[-Lb2:Lb2:nx*1j, Lb2+dx:Lb2+5*dx:5j]
        xb, yb = np.mgrid[-Lb2:Lb2:nx*1j, -Lb2-dx:-Lb2-5*dx:5j]
        x = np.hstack((xt, xb))
        y = np.hstack((yt, yb))
        x, y = (np.ravel(_arr) for _arr in (x,y))
        solid = get_particle_array_edac_solid(name='solid', x=x, y=y)

        print("KHI :: Re = %d, nfluid = %d, nsolid=%d, dt = %g"%(
            Re, fluid.get_number_of_particles(),
            solid.get_number_of_particles(), dt))

        # setup the particle properties
        volume = dx * dx

        # mass is set to get the reference density of rho0
        fluid.rho[:] = rho0
        solid.rho[:] = rho0
        fluid.m[:] = volume * rho0
        solid.m[:] = volume * rho0

        # smoothing lengths
        fluid.h[:] = hdx * dx
        solid.h[:] = hdx * dx

        return [fluid, solid]

    def create_domain(self):
        return DomainManager(xmin=0, xmax=L, periodic_in_x=True)

    def consume_user_options(self):
        self.scheme = EDACScheme(
            fluids=['fluid'], solids=['solid'], c0=c0,
            nu=nu, pb=0.0, eps=0.0, rho0=rho0
        )
        self.scheme.configure_solver(
            dim=2, kernel=CubicSpline(dim=2), dt=dt, tf=tf,
            adaptive_timestep=False
        )

    def create_solver(self):
        self.scheme.get_solver()

    def create_equations(self):
        return self.scheme.get_equations()


if __name__ == '__main__':
    app = KHI()
    app.run()
