"""Lid driven cavity using the EDAC formulation."""

# PySPH imports
from pysph.examples import cavity

# the equations
from edac import EDACScheme


c0 = cavity.c0
rho0 = cavity.rho0

class EDACLidDrivenCavity(cavity.LidDrivenCavity):
    def add_user_options(self, group):
        super(EDACLidDrivenCavity, self).add_user_options(group)
        group.add_argument(
            "--alpha", action="store", type=float, dest="alpha", default=0.5,
            help="Artificial viscosity parameter for EDAC."
        )

    def configure_scheme(self):
        self.scheme.configure(
            alpha=self.options.alpha, nu=self.nu, h=self.dx*cavity.hdx
        )
        self.scheme.configure_solver(
            tf=self.tf, dt=self.dt, pfreq=500,
            adaptive_timestep=False
        )

    def create_scheme(self):
        scheme = EDACScheme(
            fluids=['fluid'], solids=['solid'], dim=2, c0=c0, rho0=rho0,
            nu=0.0, pb=cavity.p0, eps=0.0, h=0.0,
        )
        return scheme


if __name__ == '__main__':
    app = EDACLidDrivenCavity()
    app.run()
    app.post_process(app.info_filename)
