# PySPH imports
from pysph.examples import periodic_cylinders

# the equations
from edac import EDACScheme

dt = periodic_cylinders.dt
tf = periodic_cylinders.tf
nu = periodic_cylinders.nu
c0 = periodic_cylinders.c0
rho0 = periodic_cylinders.rho0
p0 = periodic_cylinders.p0
fx = periodic_cylinders.fx

class EDACPC(periodic_cylinders.PeriodicCylinders):

    def create_scheme(self):
        scheme = EDACScheme(
            fluids=['fluid'], solids=['solid'], c0=c0, nu=nu, rho0=rho0,
            pb=p0, gx=fx, h=periodic_cylinders.h0
        )
        scheme.configure_solver(
            dim=2, tf=tf, dt=dt, adaptive_timestep=False, pfreq=500
        )
        return scheme


if __name__ == '__main__':
    app = EDACPC()
    app.run()
    app.post_process(app.info_filename)
