"""Couette flow using the transport velocity formulation of Adami et.al."""

from pysph.examples import couette

from edac import EDACScheme


nu = couette.nu
c0 = couette.c0
rho0 = couette.rho0
tf = couette.tf
dt = couette.dt


class EDACCouetteFlow(couette.CouetteFlow):

    def create_scheme(self):
        scheme = EDACScheme(
            fluids=['fluid'], solids=['channel'], dim=2, c0=c0, nu=nu,
            pb=0.0, eps=0.0, rho0=rho0
        )
        scheme.configure_solver(dt=dt, tf=tf)
        return scheme


if __name__ == '__main__':
    app = EDACCouetteFlow()
    app.run()
    app.post_process(app.info_filename)
