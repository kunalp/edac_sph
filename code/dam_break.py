"""Dam break solved with EDAC.
"""
import numpy as np

from pysph.base.kernels import QuinticSpline
from pysph.examples.dam_break_2d import DamBreak2D, DamBreak2DGeometry

from edac import EDACScheme


fluid_column_height = 2.0
fluid_column_width  = 1.0
container_height = 3.0
container_width  = 4
nboundary_layers = 4

g = 9.81
h = 0.0390
ro = 1000.0
co = 10.0 * np.sqrt(2*g*fluid_column_height)/2.0
nu = 0.0
tf = 1.0


class EDACDambreak2D(DamBreak2D):

    def create_particles(self):
        geom = DamBreak2DGeometry(
            container_width=container_width, container_height=container_height,
            fluid_column_height=fluid_column_height,
            fluid_column_width=fluid_column_width, dx=self.dx, dy=self.dx,
            ro=ro, co=co,
            with_obstacle=False)
        fluid, boundary = geom.create_particles(
            nboundary_layers=nboundary_layers, hdx=self.hdx
        )
        self.scheme.setup_properties([fluid, boundary])
        return [fluid, boundary]

    def consume_user_options(self):
        self.hdx = 1.0
        super(EDACDambreak2D, self).consume_user_options()

    def configure_scheme(self):
        self.scheme.configure(h=self.h)
        kernel = QuinticSpline(dim=2)
        dt = 0.125*self.h/co
        print("dt = %f"%dt)

        self.scheme.configure_solver(
            kernel=kernel, dt=dt, tf=tf, adaptive_timestep=False
        )

    def create_scheme(self):
        scheme = EDACScheme(
            fluids=['fluid'], solids=['boundary'], dim=2, c0=co, nu=nu,
            rho0=ro, h=h, pb=0.0, gy=-g, eps=0.0, clamp_p=True
        )
        return scheme


if __name__ == '__main__':
    app = EDACDambreak2D()
    app.run()
    app.post_process(app.info_filename)
