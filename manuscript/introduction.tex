\section{Introduction}
\label{sec:intro}

The Smoothed Particle Hydrodynamics (SPH) technique was initially developed
for astrophysical problems independently by Lucy~\cite{lucy77}, and Gingold
and Monaghan~\cite{monaghan-gingold-stars-mnras-77}. The method is grid-free
and self-adaptive. With the introduction of the weakly-compressible SPH scheme
(WCSPH) by Monaghan~\cite{sph:fsf:monaghan-jcp94}, the SPH method has been
extensively applied to incompressible fluid flow and free-surface problems
(see \cite{Shadloo16} and \cite{Violeau16} for a recent review and on the
application of SPH to industrial fluid flow problems). Alternative to the
WCSPH approach, pressure-based implicit SPH schemes like the
projection-SPH~\cite{sph:psph:cummins-rudman:jcp:1999} and
incompressible-SPH~\cite{isph:hu-adams:jcp:2007} have also been introduced.
These methods force the incompressiblity constraint ($\nabla \cdot \ten{u} =
0$) by solving a Pressure-Poisson Equation. While generally considered to be
more accurate, the implicit nature of these schemes makes it difficult to
implement and parallelize which has lead to the WCSPH approach garnering favor
within the SPH community.

The weakly-compressible formulation relies on a stiff equation of state
(usually referred as the Tait's equation of state in the SPH literature) that
generates large pressure changes for small density variations. A consequence
is that the large pressure oscillations need to be damped out, which
necessitate the use of some form of artificial viscosity. Another problem with
the WCSPH formulation is the appearance of void regions and particle clumping,
especially where the pressure is negative. This has resulted in some
researchers using problem-specific background pressure values to mitigate this
problem. The Transport Velocity Formulation (TVF) of \citet{Adami2013}
ameliorates some of the above issues by ensuring a more homogeneous
distribution of particles by introducing a background pressure field. This
background pressure is not tuned to any particular problem. In addition, the
particles are moved using an advection (transport) velocity instead of the
actual velocity. The advection velocity differs from the momentum velocity by
the constant background pressure. The motion induced by the background
pressure is corrected by introducing an additional stress term in the momentum
equation. The stiffness of the state equation is reduced by using a value of
$\gamma=1$ in the equation of state in contrast to the traditionally chosen
value of $\gamma=7$. The scheme produces excellent results for internal flows
and virtually eliminates particle clumping and void regions. The scheme also
displays reduced pressure oscillations. Unfortunately, the scheme does not
work for free-surface flows and this is a significant disadvantage.

The Entropically Damped Artificially Compressible (EDAC) method of
Clausen~\cite{Clausen2013,Clausen2013a} is an alternative to the artificial
compressibility used by the weakly-compressible formulation. This method is
similar to the kinetically reduced local Navier-Stokes method presented in
\cite{krlns:ansumali:prl:2005,krlns:karlin:pre:2006,krlns:borok:pre:2007}.
However, the EDAC scheme uses the pressure instead of the grand potential as
the thermodynamic variable and this simplifies the resulting equations. The
EDAC scheme does not rely on an equation of state that relates pressure to
density. Instead, an evolution equation for the pressure is derived based on
thermodynamic considerations. This equation includes a damping term for the
pressure which reduces pressure oscillations significantly. The scheme in its
original form does not introduce any new parameters into the simulation. There
is also no need to introduce an artificial viscosity in the momentum equation.
The method has been tested in finite-difference~\cite{Clausen2013} and
finite-element~\cite{Clausen2013a} schemes and appears to produce good
results.

In this work, the EDAC method is applied to SPH for the simulation of
incompressible fluids for both internal and free-surface problems. The
motivation for this work arose from the encouraging (despite a relatively
naive implementation) results presented in \cite{PRKP:edac-sph-iccm2015}. In
that work, we found that a simple application of the EDAC scheme produced
results that were better than the standard WCSPH, though not better than those
of the TVF scheme. Upon further investigation, it was found that when the
background pressure used in the TVF formulation is set to zero, the EDAC
scheme outperforms it. This is because the EDAC scheme provides a smoother
pressure distribution than that which is obtained via the equation of state.
There is still no mechanism within the EDAC framework to ensure a uniform
distribution of particles however. Therefore, we adapted the TVF scheme to be
used along with EDAC. The resulting scheme produces very good results and
outperforms the standard TVF for the set of benchmark problems considered in
this work.

The proposed EDAC scheme thus comes in two flavors. For internal flows, a
formulation based on the TVF is employed where a background pressure is added.
This background pressure ensures a homogeneous particle distribution. For
free-surface flows, a straight-forward formulation is used with the EDAC to
produce very good results. The scheme thus works well for both internal and
external flows. Several results are presented along with suitable comparisons
between the TVF and standard SPH schemes to demonstrate the new scheme. All
the results presented in this work are reproducible through the publicly
available PySPH package~\cite{PRKP:PySPH-particles13,PR:pysph:scipy16} and
\url{http://gitlab.com/prabhu/edac_sph}.

The paper is organized as follows. In Section~\ref{sec:edac-scheme}, the
governing equations for the EDAC scheme is outlined. In
Section.~\ref{sec:implementation}, the SPH discretization for the EDAC
equations are presented. In Section.~\ref{sec:results}, the new scheme is
evaluated against a suite benchmark problems of increasing complexity. The
results are compared to the analytical solution where available, and to the
traditional WCSPH and TVF formulations wherever possible. In
Section.~\ref{sec:conclusions}, the paper is concluded with a summary and an
outline for further work.

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "edac_sph"
%%% fill-column: 78
%%% End:
