#!/usr/bin/env python
"""A simple script to automate the results using pysph's automate and job
management tools.

To run this type:

  $ python automate.py

Configure any remote workers using the cluster.py script or editing the
config.json.

"""

import os

from pysph.tools.automation import Automator

from common import all_problems

if __name__ == '__main__':
    automator = Automator(
        simulation_dir='outputs',
        output_dir=os.path.join('manuscript', 'figures'),
        all_problems=all_problems
    )
    automator.run()
